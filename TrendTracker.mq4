//+------------------------------------------------------------------+
//|                                                 TrendTracker.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Meng Zhang"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

input int      MAGICMA = 8391389;
input double   Lots = 0.1;
input int      shortMA = 14;
input int      longMA = 55;
input int      slippage = 5;
input double   maxLoss = 60;
input double   distance = 100;

const int shiftforCur = 1;
const int shiftforPrev = 2;
const double longBound = 0.00020;
const double shortBound = 0.00045;

double STP;
double TKP;
double dist; 
bool buy;
bool sell;
double lastOrderPrice; 
double stoplevel;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
      buy = false;
      sell = false;
      dist = distance * Point; 
      lastOrderPrice = -1;
      stoplevel = 100;//MarketInfo(Symbol(), MODE_STOPLEVEL);
//---
      return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
{
//---
      if(Bars<100 || IsTradeAllowed()==false)
         return;
         
      if(Volume[0]>1)
      {
         if(buy)
         {
            Buy();
         }
         else if(sell)
         {
            Sell();
         }
         return;
      }
      CheckClose();
      CheckOpen();
      if(buy)
      {
         Buy();
      }
      else if(sell)
      {
         Sell();
      }
}

void Buy()
{     
   if(lastOrderPrice > 0)
   {
      if(Ask - lastOrderPrice > dist)
      {
         BuyMore();
      }
      return;
   }

   //TODO: qian gao qian di 
   //open buy order
   //double stoploss = NormalizeDouble(LastLocalLowest(), Digits);
   //double takeprofit = NormalizeDouble(LastLocalHighest(), Digits); 
   BuyFirst();   
}

void BuyFirst()
{
   double buyprice = Ask;
   double stoploss = NormalizeDouble(Ask - stoplevel * 1.5 * Point, Digits);
   double takeprofit = NormalizeDouble(Ask + (500+stoplevel) * Point, Digits);

   int ticket = OrderSend(Symbol(), OP_BUY, Lots, buyprice, slippage, stoploss, takeprofit, "", MAGICMA, 0, Blue);

   if(ticket<0) 
   { 
      Print("Buy OrderSend failed with error #",GetLastError()); 
   } 
   else
   { 
      lastOrderPrice = Ask;
      Print("Buy OrderSend placed successfully");
   }
}

void BuyMore()
{
   double buyprice = Ask;
   double stoploss = NormalizeDouble(Ask - stoplevel * Point, Digits);
   double takeprofit = NormalizeDouble(Ask + (500+stoplevel) * Point, Digits);
   
   int ticket = OrderSend(Symbol(), OP_BUY, Lots, buyprice, slippage, stoploss, 0, "", MAGICMA, 0, Blue);

   if(ticket<0) 
   { 
      Print("Buy OrderSend failed with error #",GetLastError()); 
   } 
   else
   { 
      lastOrderPrice = Ask;
      Print("Buy OrderSend placed successfully");
   }
   
   for(int i=0; i<OrdersTotal(); i++)
   {
      if(OrderSelect(i, SELECT_BY_POS, MODE_TRADES)==false)
      {
         break;
      }
      
      if(OrderSymbol() == Symbol() && OrderMagicNumber() == MAGICMA)
      {
         if(OrderType() == OP_BUY)  
         {
            bool res = OrderModify(OrderTicket(), OrderOpenPrice(), stoploss, takeprofit, 0, Blue);
            if(!res) 
            {
               Print("Error in OrderModify. Error code=", GetLastError()); 
            }
            else
            { 
               lastOrderPrice = Ask;
               Print("Order modified successfully."); 
            }
         }
      }
   }
   
}

void Sell()
{     
   if(lastOrderPrice > 0)
   {
      if(lastOrderPrice - Bid > dist)
      {
         SellMore();
      }
      return;
   }

   //TODO: qian gao qian di 
   //open buy order
   //double stoploss = NormalizeDouble(LastLocalLowest(), Digits);
   //double takeprofit = NormalizeDouble(LastLocalHighest(), Digits); 
   SellFirst();   
}

void SellFirst()
{
   double sellprice = Bid;
   double stoploss = NormalizeDouble(Bid + stoplevel * 1.5 * Point, Digits);
   double takeprofit = NormalizeDouble(Bid - (500+stoplevel) * Point, Digits);

   int ticket = OrderSend(Symbol(), OP_SELL, Lots, sellprice, slippage, stoploss, takeprofit, "", MAGICMA, 0, Red);

   if(ticket<0) 
   { 
      Print("Sell OrderSend failed with error #",GetLastError()); 
   } 
   else
   { 
      lastOrderPrice = Bid;
      Print("Sell OrderSend placed successfully");
   }
}

void SellMore()
{
   double sellprice = Bid;
   double stoploss = NormalizeDouble(Bid + stoplevel * Point, Digits);
   double takeprofit = NormalizeDouble(Bid - (500+stoplevel) * Point, Digits);
   
   int ticket = OrderSend(Symbol(), OP_SELL, Lots, sellprice, slippage, stoploss, 0, "", MAGICMA, 0, Red);

   if(ticket<0) 
   { 
      Print("Sell OrderSend failed with error #",GetLastError()); 
   } 
   else
   { 
      lastOrderPrice = Bid;
      Print("Buy OrderSend placed successfully");
   }
   
   for(int i=0; i<OrdersTotal(); i++)
   {
      if(OrderSelect(i, SELECT_BY_POS, MODE_TRADES)==false)
      {
         break;
      }
      
      if(OrderSymbol() == Symbol() && OrderMagicNumber() == MAGICMA)
      {
         if(OrderType() == OP_SELL)  
         {
            bool res = OrderModify(OrderTicket(), OrderOpenPrice(), stoploss, takeprofit, 0, Red);
            if(!res) 
            {
               Print("Error in OrderModify. Error code=", GetLastError()); 
            }
            else
            { 
               lastOrderPrice = Bid;
               Print("Order modified successfully."); 
            }
         }
      }
   }
   
}

void CheckOpen()
{  
   if(buy || sell)
      return;
   
   double longCur = iMA(Symbol(), 0, longMA, 0, MODE_SMA, PRICE_CLOSE, shiftforCur);     
   double shortCur = iMA(Symbol(), 0, shortMA, 0, MODE_SMA, PRICE_CLOSE, shiftforCur);
   
   double shortPrev = iMA(Symbol(), 0, shortMA, 0, MODE_SMA, PRICE_CLOSE, shiftforPrev);
   double longPrev = iMA(Symbol(), 0, longMA, 0, MODE_SMA, PRICE_CLOSE, shiftforPrev);
   
   // long trend is increasing and short increasing trend is stronger.
   if(longCur - longPrev > longBound && shortCur - shortPrev > longCur - longPrev)
   {
      buy = true;
      return;
   }
   
   // long trend is horizontal, and short signal is strongly increasing.
   if(longCur - longPrev < longBound && longCur - longPrev > -1 * longBound && shortCur - shortPrev > shortBound)
   {
      buy = true;
      return;
   }
   
   if(longPrev - longCur > longBound && shortPrev - shortCur > longPrev - longCur)
   {
      sell = true;
      return;
   }   
   
   if(longCur - longPrev < longBound && longCur - longPrev > -1 * longBound && shortPrev - shortCur > shortBound)
   {
      sell = true;
      return;
   }

}

void CheckClose()
{  
   double shortCur = iMA(Symbol(), 0, shortMA, 0, MODE_SMA, PRICE_CLOSE, shiftforCur); 
   double shortPrev = iMA(Symbol(), 0, shortMA, 0, MODE_SMA, PRICE_CLOSE, shiftforPrev); 
   
   if(shortCur <= shortPrev && buy)
   {
      CloseBuy();
   }  
   
   if(shortCur >= shortPrev && sell)
   {
      CloseSell();
   }
}

void CloseBuy()
{
   for(int i = 0; i < OrdersTotal(); i++)
   {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false)
      {
         break;
      }
      if(OrderMagicNumber() != MAGICMA || OrderSymbol() != Symbol()) 
      {
         continue;
      }
         
      if(OrderType() == OP_BUY)
      {
         if(!OrderClose(OrderTicket(),OrderLots(),Bid,slippage,White))
         {
            Print("OrderClose error ",GetLastError());
         }
         break;
      }
   }
   
   buy = false;
   
   lastOrderPrice = -1;
}

void CloseSell()
{
   for(int i = 0; i < OrdersTotal(); i++)
   {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false)
      {
         break;
      }
      if(OrderMagicNumber() != MAGICMA || OrderSymbol() != Symbol()) 
      {
         continue;
      }
         
      if(OrderType() == OP_SELL)
      {
         if(!OrderClose(OrderTicket(),OrderLots(),Ask,slippage,White))
         {
            Print("OrderClose error ",GetLastError());
         }
         break;
      }
   }
   
   sell = false;
   
   lastOrderPrice = -1;
}

double LastLocalLowest()
{
   return 0;
}

double LastLocalHighest()
{
   return 0;
}
//+------------------------------------------------------------------+
