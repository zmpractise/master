//+------------------------------------------------------------------+
//|                                                  FollowTrend.mq4 |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "zhang meng"
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//--- input parameters
input int MAGICMA = 20666666;
input double   Lots = 0.1;
input int      shortMA = 13;
input int      mediumMA = 34;
input int      longMA = 55;
input int      slippage = 5;
input double   maxLoss = 60;
input double   distance = 100;
input double   longDifference = 0;//0.00012;
//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
double STP;
double TKP;
double dist; 
double longDiff;
double lastOrderPrice;
bool buy;
bool sell;

const int shiftforCur = 1;
const int shiftforPrev = 2;
const double strongSignal = 0.0006;

int OnInit()
  {
//---
      STP = maxLoss;
      TKP = 200;
      dist = distance * Point;
      longDiff = longDifference;
      if(_Digits == 4)
      {
         STP = STP / 10;
         TKP = TKP / 10;
         dist = dist / 10;
         longDiff = longDiff / 10;
      }
      buy = false;
      sell = false;
      lastOrderPrice = -1;
//---
      return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
      if(Bars<100 || IsTradeAllowed()==false)
         return;
         
      if(Volume[0]>1)
      { 
         if(GetBuy())
         {
            BuyOrModify();
         }
         
         if(GetSell())
         {
            SellOrModify();
         }
         return;
      }
         
      CheckClose();
      DecideOpenAndModify();
      
  }
//+------------------------------------------------------------------+

void BuyOrModify()
{
   //open buy order
   double stoploss = NormalizeDouble(Ask - STP*Point, Digits);
   double takeprofit = NormalizeDouble(Ask + TKP*Point, Digits);
 
   if(lastOrderPrice > 0 && Ask - lastOrderPrice < dist)
   {
      return;
   }
   int ticket = OrderSend(Symbol(), OP_BUY, Lots, Ask, slippage, stoploss, takeprofit, "", MAGICMA, 0, Blue);
   
   double tempPrice = Ask; 
   if(ticket<0) 
   { 
      Print("Buy OrderSend failed with error #",GetLastError()); 
   } 
   else
   { 
      lastOrderPrice = tempPrice;
      Print("Buy OrderSend placed successfully");
   }
   
   for(int i=0; i<OrdersTotal(); i++)
   {
      if(OrderSelect(i, SELECT_BY_POS, MODE_TRADES)==false)
      {
         break;
      }
      
      if(OrderSymbol() == Symbol() && OrderMagicNumber() == MAGICMA)
      {
         if(OrderType() == OP_BUY)  
         {
            bool res = OrderModify(OrderTicket(), OrderOpenPrice(), stoploss, takeprofit, 0, Blue);
            if(!res) 
            {
               Print("Error in OrderModify. Error code=", GetLastError()); 
            }
            else
            { 
               lastOrderPrice = tempPrice;
               Print("Order modified successfully."); 
            }
         }
      }
   }      
}

void SellOrModify()
{
//open sell order
   double stoploss = NormalizeDouble(Bid + STP*Point, Digits);
   double takeprofit = NormalizeDouble(Bid - TKP*Point, Digits);
 
   if(lastOrderPrice > 0 && lastOrderPrice - Bid < dist)
   {
      return;
   }
   int ticket = OrderSend(Symbol(), OP_SELL, Lots, Bid, slippage, stoploss, takeprofit, "", MAGICMA, 0, Red);
   
   double tempPrice = Bid; 
   if(ticket<0) 
   { 
      Print("Sell OrderSend failed with error #",GetLastError()); 
   } 
   else
   { 
      lastOrderPrice = tempPrice;
      Print("Sell OrderSend placed successfully");
   }
   
   for(int i=0; i<OrdersTotal(); i++)
   {
      if(OrderSelect(i, SELECT_BY_POS, MODE_TRADES)==false)
      {
         break;
      }
      
      if(OrderSymbol() == Symbol() && OrderMagicNumber() == MAGICMA)
      {
         if(OrderType() == OP_SELL)  
         {
            bool res = OrderModify(OrderTicket(), OrderOpenPrice(), stoploss, takeprofit, 0, Red);
            if(!res) 
            {
               Print("Error in OrderModify. Error code=", GetLastError()); 
            }
            else
            { 
               lastOrderPrice = tempPrice;
               Print("Order modified successfully."); 
            }
         }
      }
   }
}

void CloseBuy()
{
   lastOrderPrice = -1;
   for(int i=0;i<OrdersTotal();i++)
   {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false)
      {
         break;
      }
      if(OrderMagicNumber()!=MAGICMA || OrderSymbol()!=Symbol()) 
      {
         continue;
      }
         
      if(OrderType()==OP_BUY)
      {
         if(!OrderClose(OrderTicket(),OrderLots(),Bid,slippage,White))
         {
            Print("OrderClose error ",GetLastError());
         }
         break;
      }
   }
}

void CloseSell()
{
   lastOrderPrice = -1;
   for(int i=0;i<OrdersTotal();i++)
   {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false)
      {
         break;
      }
      if(OrderMagicNumber()!=MAGICMA || OrderSymbol()!=Symbol()) 
      {
         continue;
      }
         
      if(OrderType()==OP_SELL)
      {
         if(!OrderClose(OrderTicket(),OrderLots(),Ask,slippage,White))
         {
            Print("OrderClose error ",GetLastError());
         }
         break;
      }
   }
}

void CheckClose()
{
   double longCur = iMA(Symbol(), 0, longMA, 0, MODE_SMA, PRICE_CLOSE, shiftforCur);  
   double mediumCur = iMA(Symbol(), 0, mediumMA, 0, MODE_SMA, PRICE_CLOSE, shiftforCur);
   double shortCur = iMA(Symbol(), 0, shortMA, 0, MODE_SMA, PRICE_CLOSE, shiftforCur);
   
   // long trend is increasing
   if(longCur < mediumCur && longCur < shortCur)
   {
      double shortPrev = iMA(Symbol(), 0, shortMA, 0, MODE_SMA, PRICE_CLOSE, shiftforPrev);
      
      //short is decreasing
      if(shortPrev >= shortCur)
      {
         CloseBuy();
      }
      
      return;
   }
   
   // long is decreasing
   if(longCur > mediumCur && longCur > shortCur)
   {
      double shortPrev = iMA(Symbol(), 0, shortMA, 0, MODE_SMA, PRICE_CLOSE, shiftforPrev);
      //short is increasing
      if(shortPrev <= shortCur)
      {
         CloseSell();
      }
   } 
   
}

void DecideOpenAndModify()
{
   double longCur = iMA(Symbol(), 0, longMA, 0, MODE_SMA, PRICE_CLOSE, shiftforCur);  
   double mediumCur = iMA(Symbol(), 0, mediumMA, 0, MODE_SMA, PRICE_CLOSE, shiftforCur);
   double shortCur = iMA(Symbol(), 0, shortMA, 0, MODE_SMA, PRICE_CLOSE, shiftforCur);
   double shortPrev = iMA(Symbol(), 0, shortMA, 0, MODE_SMA, PRICE_CLOSE, shiftforPrev);
   double mediumPrev = iMA(Symbol(), 0, mediumMA, 0, MODE_SMA, PRICE_CLOSE, shiftforPrev);
   double longPrev = iMA(Symbol(), 0, longMA, 0, MODE_SMA, PRICE_CLOSE, shiftforPrev);
/*   Print("long: ", longPrev, "---long cur: ", longCur);
   Print("medium: ", mediumPrev, "---medium cur: ", mediumCur);
   Print("short: ", shortPrev, "----short cur: ", shortCur);
   */
   if(((longCur < mediumCur && mediumCur < shortCur) && (shortPrev < shortCur && mediumPrev < mediumCur && longPrev + longDiff < longCur)) || shortCur - shortPrev > strongSignal)
   {
      SetBuy(true);
   }
   else
   {
      SetBuy(false);
   }
   
   if(((longCur > mediumCur && mediumCur > shortCur) && (shortPrev > shortCur && mediumPrev > mediumCur && longPrev - longDiff > longCur)) || shortPrev - shortCur > strongSignal)
   {
      SetSell(true);
   }
   else
   {
      SetSell(false);
   }
}

bool GetBuy()
{
   return buy;
}

bool GetSell()
{
   return sell;
}

void SetBuy(bool value)
{
   Print("set buy ", value);
   buy = value;
}

void SetSell(bool value)
{
   sell = value;
}